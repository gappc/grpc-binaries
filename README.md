# grpc - compiled binaries for memetank-app

This project contains compiled grpc binaries to speed up the npm installation procedure.

Refer to the binaries during npm install with the `--grpc_node_binary_host_mirror` parameter, e.g.:

```bash
npm i --grpc_node_binary_host_mirror=https://gitlab.com/gappc/grpc-binaries/-/raw/master/
```
